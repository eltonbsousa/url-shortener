# Instructions

- Clone repository
```
git clone https://eltonbsousa@bitbucket.org/eltonbsousa/url-shortener.git
cd url-shortener
```

- Start Docker
```
docker-compose build
docker-compose up -d
```

- Generate assets
```
docker-compose exec php yarn install
docker-compose exec php yarn build
```

- Run migrations
```
docker-compose exec php bin/console doctrine:migrations:migrate
```

- Access `http:\\localhost`
