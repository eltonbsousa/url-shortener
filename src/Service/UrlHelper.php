<?php

namespace App\Service;

class UrlHelper
{
    public function generateShort($i): ?string
    {
        $alphabet = str_split("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");

        if ($i === 0) {
            return $alphabet[0];
        }

        $s = '';
        $base = sizeof($alphabet);

        while ($i > 0) {
            $s .= $alphabet[$i % $base];
            $i = (int) ($i / $base);
        }

        return strrev($s);
    }
}
