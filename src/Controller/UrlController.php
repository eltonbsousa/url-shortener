<?php

namespace App\Controller;

use App\Entity\Url;
use App\Form\UrlType;
use App\Repository\UrlRepository;
use App\Service\UrlHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class UrlController extends AbstractController
{
    #[Route('/', name: 'url_index', methods: ['GET|POST'])]
    public function index(Request $request, EntityManagerInterface $entityManager, UrlRepository $urlRepository, UrlHelper $urlHelper): Response
    {
        $url = new Url();

        $form = $this->createForm(UrlType::class, $url);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Url $urlExist */
            $urlExist = $urlRepository->findOneBy(['orginal' => $url->getOrginal()]);

            $id = null;
            if (null !== $urlExist) {
                $urlExist->setAccess($urlExist->getAccess() +1);
                $id = $urlExist->getId();
            } else {
                $url->setAccess(1);
                $entityManager->persist($url);
                $entityManager->flush();

                $url->setShort($urlHelper->generateShort($url->getId()));

                $id = $url->getId();
            }

            $entityManager->flush();

            return $this->redirectToRoute('url_show', ['id' => $id], Response::HTTP_SEE_OTHER);
        }

        return $this->render('url/index.html.twig', [
            'urls' => $urlRepository->findAll(),
            'url' => $url,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/show/{id}', name: 'url_show', methods: ['GET'])]
    public function show(Url $url): Response
    {
        return $this->render('url/show.html.twig', [
            'url' => $url,
        ]);
    }

    #[Route('/{short}', name: 'url_short', methods: ['GET'])]
    public function short(Request $request, UrlRepository $urlRepository): Response
    {
        $shortStr = $request->get('short');

        /** @var Url $url */
        $url = $urlRepository->findOneBy(['short' => $shortStr]);

        if (null === $url) {
           return $this->render('url/not_found.html.twig');
        }

        return $this->redirect($url->getOrginal());
    }
}
