<?php

namespace App\Entity;

use App\Repository\UrlRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UrlRepository::class)]
class Url
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    #[Assert\Url]
    private $orginal;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $short;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $title;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $access;

    public function __toString(): string
    {
        return $this->orginal ?? '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrginal(): ?string
    {
        return $this->orginal;
    }

    public function setOrginal(string $orginal): self
    {
        $this->orginal = $orginal;

        return $this;
    }

    public function getShort(): ?string
    {
        return $this->short;
    }

    public function setShort(string $short): self
    {
        $this->short = $short;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAccess(): ?int
    {
        return $this->access;
    }

    public function setAccess(?int $access): self
    {
        $this->access = $access;

        return $this;
    }
}
